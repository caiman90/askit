import React from 'react';
import Typography from '@material-ui/core/Typography';

const HeadlineComponent = ({title}) => {
    return (
        <Typography color="primary" style={{fontSize:24, margin:'30px 0px 30px 0px' }}>{title}</Typography>
    );
};

export default HeadlineComponent;



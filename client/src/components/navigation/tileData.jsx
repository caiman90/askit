// This file is shared across the demos.

import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import StarIcon from '@material-ui/icons/Star';
import SendIcon from '@material-ui/icons/Send';
import HomeIcon from '@material-ui/icons/Home';
import NoteAdd from '@material-ui/icons/NoteAdd';
import GroupAdd from '@material-ui/icons/GroupAdd';
import { Link } from 'react-router-dom';

export const MailFolderListItems = ({ logout }) => {
  return (
    <div>
       <Link href to="/" style={{display:"flex",textDecoration:"none"}}>
         <ListItem button>
          <ListItemIcon>
            <HomeIcon />
          </ListItemIcon>
         <ListItemText primary="Home" />
       </ListItem>
       </Link>

        <Link href to="/questions" style={{display:"flex",textDecoration:"none"}}>
        <ListItem button>
            <ListItemIcon>
                <NoteAdd />
            </ListItemIcon>
            <ListItemText primary="Questions" />
        </ListItem>
        </Link>

        <Link href to="/myquestions" style={{display:"flex",textDecoration:"none"}}>
        <ListItem button>
            <ListItemIcon>
                <InboxIcon />
            </ListItemIcon>
            <ListItemText primary="My Questions" />
        </ListItem>
        </Link>

        <Link href to="/profile" style={{display:"flex",textDecoration:"none"}}>
        <ListItem button>
            <ListItemIcon>
                <StarIcon />
            </ListItemIcon>
            <ListItemText primary="Profile" />
        </ListItem>
        </Link>

        <Link href to="/login" onClick={logout} style={{display:"flex",textDecoration:"none"}}>
        <ListItem button>
            <ListItemIcon>
                <SendIcon />
            </ListItemIcon>
            <ListItemText primary="Logout" />
        </ListItem>
        </Link>
    </div>
);}
export const notLoggedInMailFolderListItems =(
    <div>
      <Link href to="/" style={{display:"flex",textDecoration:"none"}}>
        <ListItem button>
            <ListItemIcon>
                <HomeIcon />
            </ListItemIcon>
            <ListItemText primary="Home" />
        </ListItem>
       </Link>
        <Link href to="/questions" style={{display:"flex",textDecoration:"none"}}>
          <ListItem button>
              <ListItemIcon>
                <InboxIcon />
              </ListItemIcon>
          < ListItemText primary="Questions" />
            </ListItem>
        </Link>
          <Link href to="/login" style={{display:"flex",textDecoration:"none"}}>
            <ListItem button>
                <ListItemIcon>
                    <StarIcon />
                </ListItemIcon>
                <ListItemText primary="Login" />
            </ListItem>
        </Link>

        <Link href to="/register" style={{display:"flex",textDecoration:"none"}}>
            <ListItem button>
                <ListItemIcon>
                    <GroupAdd />
                </ListItemIcon>
                <ListItemText primary="Register" />
            </ListItem>
        </Link>
    </div>
);

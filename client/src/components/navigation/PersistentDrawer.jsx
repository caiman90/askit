import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import { Route, Switch } from 'react-router-dom';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { MailFolderListItems,notLoggedInMailFolderListItems } from './tileData';
import LoginPage from '../login/LoginPage';
import MyQuestionsPage from '../myquestions/MyQuestionsPage';
import QuestionsPage from '../questions/QuestionsPage';
import ProfilePage from '../profile/ProfilePage';
import RegisterPage from '../register/RegisterPage';
import HomePage from '../home/HomePage';
import ResetPasswordLinkComponent from '../login/ResetPasswordLinkComponent';
import ResetPasswordComponent from '../login/ResetPasswordComponent';
import {connect} from 'react-redux';
import {logout} from '../../actions/auth.actions';
import PrivateRoute from '../common/PrivateRoute';
import { withRouter } from 'react-router-dom';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appFrame: {
        height: '100%',
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        position: 'absolute',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'appBarShift-left': {
        marginLeft: drawerWidth,
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    'content-left': {
        marginLeft: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'contentShift-left': {
        marginLeft: 0,
    }
});

class PersistentDrawer extends React.Component {


    state = {
        open: false,
        anchor: 'left',
    };

    handleDrawerOpen = () => {
        this.setState({ open: true });
    };

    handleDrawerClose = () => {
        this.setState({ open: false });
    };


    render() {
        const { classes } = this.props;
        const { anchor, open } = this.state;

        const isAuthenticated = this.props.auth.isAuthenticated;
        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar
                        className={classNames(classes.appBar, {
                            [classes.appBarShift]: open,
                            [classes[`appBarShift-${anchor}`]]: open,
                        })}
                    >
                        <Toolbar disableGutters={!open}>
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                onClick={this.handleDrawerOpen}
                                className={classNames(classes.menuButton, open && classes.hide)}
                            >
                                <MenuIcon />
                            </IconButton>
                            <Typography variant="title" color="inherit" noWrap>
                                AskIt
                            </Typography>
                        </Toolbar>
                    </AppBar>
                    <Drawer
                        variant="persistent"
                        anchor={anchor}
                        open={open}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                    >
                        <div className={classes.drawerHeader}>
                            <IconButton onClick={this.handleDrawerClose}>
                             <ChevronLeftIcon />
                            </IconButton>
                        </div>
                        <Divider />

                        { isAuthenticated
                            ?    <div><List><MailFolderListItems logout={this.props.logout}  /></List> </div>
                            :    <div><List>{notLoggedInMailFolderListItems}</List></div>
                        }


                    </Drawer>
                    <main
                        className={classNames(classes.content, classes[`content-${anchor}`], {
                            [classes.contentShift]: open,
                            [classes[`contentShift-${anchor}`]]: open,
                        })}
                     >
                        <div className={classes.drawerHeader} />
                            <div>
                            <Switch>
                                <Route exact path="/" component={HomePage} />
                                <Route exact path="/login" component={LoginPage} />
                                <Route exact path="/questions" component={QuestionsPage} />
                                <PrivateRoute exact path="/profile" component={ProfilePage} />
                                <Route exact path="/register" component={RegisterPage} />
                                <PrivateRoute exact path="/myquestions" component={MyQuestionsPage} />
                                <Route exact path="/resetpassword" component={ResetPasswordLinkComponent} />
                                <Route path="/reset/:token" component={ResetPasswordComponent} />
                            </Switch>
                            </div>
                    </main>
                </div>
            </div>
        );
    }
}

PersistentDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = state => ({
    auth: state.auth
});
const styledComponent = withStyles(styles, { withTheme: true })(PersistentDrawer);
export default withRouter(connect(mapStateToProps,{logout})(styledComponent));

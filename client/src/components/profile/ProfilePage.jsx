import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

import {connect} from 'react-redux';
import Paper from '@material-ui/core/Paper';
import { Card,CardContent } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import { getProfileData } from '../../actions/auth.actions';
import HeadlineComponent from '../common/HeadlineComponent';
import Avatar from '@material-ui/core/Avatar';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { uploadUserImage } from "../../actions/auth.actions";
import PropTypes from "prop-types";

const styles = theme => ({
    row: {
        display: 'flex',
        justifyContent: 'center',
    },
    avatar: {
        margin: 10,
    },
    bigAvatar: {
        width: 60,
        height: 60,
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
});

class ProfilePage extends Component {
  constructor(){
      super();
      this.state = { selectedFile: {}};
      this.uploadImage = this.uploadImage.bind(this);
      this.fileChangedHandler = this.fileChangedHandler.bind(this);
  }
  uploadImage(){
      if(this.state.selectedFile){
         this.props.uploadUserImage(this.state.selectedFile);
      }
  }
  fileChangedHandler(event){
      this.setState({selectedFile: event.target.files[0]})
  }
  componentDidMount(){
   this.props.getProfileData();
  }
  render() {
      const { classes } = this.props;
      return (
      <Grid container justify="center" style={{marginTop:50}}>
      <Paper style={{width:'100%'}}>

          <HeadlineComponent title={'USER PROFILE'} />
          <div style={{display: 'flex',flex:1}}>
              <input
                  accept="image/*"
                  className={classes.input}
                  id="contained-button-file"
                  type="file"
                  onChange={this.fileChangedHandler}
              />
              <label htmlFor="contained-button-file">
                  <Button variant="contained" component="span" className={classes.button}>
                      Upload
                      <CloudUploadIcon className={classes.rightIcon} />
                  </Button>

              </label>
          <Typography color="primary" style={{fontSize:20, margin: '15px 10px 0px 20px'}}>{this.state.selectedFile.name}</Typography>
          <Button variant="contained" component="span" className={classes.button} onClick={this.uploadImage}>Upload Profile Image</Button>
          </div>
          <Card>
                <CardContent>
                    <div style={{display: 'flex'}}>
                        <Avatar
                        alt={this.props.user.firstName}
                        src={this.props.user.imageUrl}
                        className={classNames(classes.avatar, classes.bigAvatar)}
                    /> <div>
                        <Typography> FIRST NAME : {this.props.user.firstName}</Typography>
                        <Typography> USER EMAIL : {this.props.user.email}</Typography>
                        <Typography> LAST NAME  : {this.props.user.lastName}</Typography>
                        <Typography> NUM_OF_QUESTIONS: {this.props.user.num_of_questions}</Typography>
                        <Typography> NUM_OF_ANSWERS : {this.props.user.num_of_answers}</Typography>
                    </div>
                    </div>
                </CardContent>
              </Card>
          </Paper>
        </Grid>
     );
  }
}
  ProfilePage.propTypes = {
    user: PropTypes.object.isRequired
  }
  const mapStateToProps = state => ({
    user: state.auth.user
  });
  export default connect(mapStateToProps,{ getProfileData, uploadUserImage })(withStyles(styles)(ProfilePage));

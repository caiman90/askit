import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from  '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import { Link } from 'react-router-dom';
import { login } from '../../actions/auth.actions';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

class LoginFormComponent extends Component {
    constructor(){
      super();
      this.state = {
        formData: {
          email:'',
          password:'',
        },
        errorsText:'',
      }
      this.onSubmit = this.onSubmit.bind(this);
      this.onChange = this.onChange.bind(this);
      this.handleBlur = this.handleBlur.bind(this);
    }
    componentWillMount(){
        ValidatorForm.addValidationRule('minLength', (value) => {
           return value.length < 6 ? false : true;
        });
    }
    componentDidMount() {
      if (this.props.auth.isAuthenticated) {
        this.props.history.push('/questions');
      }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push('/questions');
    }

    if (nextProps.auth.error) {
      this.setState({ errorsText: nextProps.auth.error.message});
    }
  }
   onSubmit(event){
     event.preventDefault();
     this.props.login(this.state.formData);
   }
   onChange(event){
     const { formData } = this.state;
     formData[event.target.name] = event.target.value;
     this.setState({ formData });
     if(this.state.errorsText !== ''){
       this.setState({ errorsText: '' });
     }
   }
   handleBlur(event){
     this.refs[event.target.name].validate(event.target.value);
   }
   render(){
     let errorScreen = this.state.errorsText !== '' ? <Typography style={{color:'red' ,fontSize:17,border: 1,}}> {this.state.errorsText }</Typography> : '';
     const { formData } = this.state;
     return (
       <Grid container justify="center" style={{marginTop:100}}>
       <ValidatorForm ref="form" onSubmit={this.onSubmit}  instantValidate={false}>
              <TextValidator
                 label="Username"
                 name="email"
                 ref="email"
                 value={formData.email}
                 onChange={this.onChange}
                 onBlur={this.handleBlur}
                 margin="normal"
                 validators={['required', 'isEmail']}
                 errorMessages={['This field is required', 'Email is not valid']}
               />
               <br/> <br/>
               <TextValidator
                  label="Password"
                  name="password"
                  ref="password"
                  onBlur={this.handleBlur}
                  value={formData.password}
                  onChange={this.onChange}
                  margin="normal"
                  type="password"
                  validators={['required','minLength']}
                  errorMessages={['This field is required','Password min length is 6']}
               />
               < br/> <br />
               <Button type="submit" color="primary" style={{ width: '180px'}}>Sign In</Button>
               <br />
               {errorScreen}
                <br />
               <Link href to="/resetpassword"><Button  style={{ width: '180px'}} color="primary">Forgot Password ? </Button></Link>

         </ValidatorForm>
         </Grid>

     );
   }
}

LoginFormComponent.propTypes = {
  login: PropTypes.func.isRequired,
}
const mapStateToProps = state => ({
   auth : state.auth,
});
export default connect(mapStateToProps, { login })(withRouter(LoginFormComponent));

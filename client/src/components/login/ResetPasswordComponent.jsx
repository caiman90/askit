import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from  '@material-ui/core/Button';
import { resetPassword } from '../../actions/auth.actions';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

class ResetPasswordComponent extends Component {
  constructor(){
    super();
    this.state = {
      password:'',
      repeatPassword:'',
      errorsText:''
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }
  componentWillReceiveProps(nextProps){
    if (nextProps.auth.error) {
      this.setState({ errorsText: nextProps.auth.error});
    }else{
      alert("A password has been sucessfully reseted. Please log in with new password")
      this.props.history.push('/login');
    }
  }
componentWillMount(){
    ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
          if (value !== this.state.password) {
              return false;
          }
          return true;
      });
      ValidatorForm.addValidationRule('minLength', (value) => {
            if (value.length < 6) {
                return false;
            }
            return true;
        });
  }
  onChange(event){
    this.setState({ [event.target.name]: event.target.value });
  }
  onSubmit(event){
    this.props.resetPassword({password: this.state.password,token:this.props.match.params.token});
  }
  onBlur(event){
    this.refs[event.target.name].validate(event.target.value);
  }
  render() {
    let errorScreen = this.state.errorsText !== '' ? <Typography style={{color:'red' ,fontSize:17,border: 1,}}> {this.state.errorsText }</Typography> : '';
    return (
      <div>
      <Grid container justify="center" style={{marginTop:100}}>
      <ValidatorForm  ref="form"  onSubmit={this.onSubmit} instantValidate={false}>
      <Typography color="primary" style={{fontSize:24}}>Reset Password</Typography>
      <TextValidator
        placeholder="Password"
        name="password"
        ref="password"
        value={this.state.password}
        onChange={this.onChange}
        onBlur={this.onBlur}
        margin="normal"
        type="password"
        validators={['required']}
        errorMessages={['This field is required']}
      /> <br /><br />
      <TextValidator
        placeholder="Repeat Password"
        name="repeatPassword"
        ref="repeatPassword"
        value={this.state.repeatPassword}
        onChange={this.onChange}
        onBlur={this.onBlur}
        margin="normal"
        type="password"
        validators={['isPasswordMatch', 'required','minLength']}
        errorMessages={['Password mismatch', 'this field is required','Password min length is 6']}
      /> <br /><br />
      <Button type="submit" color="primary" style={{ width: '180px'}}>Send</Button>
      {errorScreen}
    </ValidatorForm>
      </Grid>
      </div> );
  }
}
ResetPasswordComponent.propTypes = {
  resetPassword: PropTypes.func.isRequired,
}
const mapStateToProps = state => ({
   auth : state.auth,
});
export default connect(mapStateToProps, { resetPassword })(ResetPasswordComponent);

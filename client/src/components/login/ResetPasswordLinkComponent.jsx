import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from  '@material-ui/core/Button';
import { resetPasswordLink } from '../../actions/auth.actions';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

class ResetPasswordLinkComponent extends Component {
  constructor(){
    super();
    this.state = {
      email:'',
      submitted:false,
      errorsText:''
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.error) {
      this.setState({ errorsText: nextProps.auth.error});
    }else{
      alert("A reset password link was sent to your email.")
      this.props.history.push('/');
    }
  }
  onChange(e){
    this.setState({ [e.target.name]: e.target.value  });
    if(this.state.errorsText !== ''){
      this.setState({ errorsText: '' });
    }
  }
  onSubmit(e){
    this.props.resetPasswordLink(this.state.email);
  }
  render() {
    let errorScreen = this.state.errorsText !== '' ? <Typography style={{color:'red' ,fontSize:17,border: 1,}}> {this.state.errorsText }</Typography> : '';
    return (
      <div>
      <Grid container justify="center" style={{marginTop:100}}>
      <ValidatorForm ref="form" onSubmit={this.onSubmit}  instantValidate={false}>
      <Typography color="primary" style={{fontSize:24}}>Reset Password</Typography>
      <TextValidator
        placeholder="Email"
        name="email"
        value={this.state.email}
        onChange={this.onChange}
        validators={['isEmail','required', ]}
        errorMessages={['Email is not valid','This field is required']}
        margin="normal"

      /> <br /><br />
      <Button type="submit" color="primary" style={{ width: '180px'}}>Send</Button>
      {errorScreen}
    </ValidatorForm>
      </Grid>
      </div> );
  }
}
ResetPasswordLinkComponent.propTypes = {
  resetPasswordLink: PropTypes.func.isRequired,
}
const mapStateToProps = state => ({
   auth : state.auth,
});
export default connect(mapStateToProps, { resetPasswordLink })(ResetPasswordLinkComponent);

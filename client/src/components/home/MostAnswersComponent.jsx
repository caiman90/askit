import React from 'react';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';

const MostAnswersComponent = ({user}) => {
  return (
           <Card style={{width: '300px',wordWrap:'break-word',float:'left',margin:'0px 20px 20px 0px'}} >
                  <CardContent>
                     <div style={{display: 'flex'}}>
                                          <Avatar
                                          alt={user.firstName}
                                          src={user.imageUrl}
                                          style={{ width: 50,height: 50, margin:5}}
                                            />
                                            <div>
                  <Typography>{user.firstName} {user.lastName}</Typography>
                  <Typography>{user.email}</Typography>
                  <Typography>NumOfAnswers:{user.num_of_answers}</Typography>
                  </div>
                  </div>
                  </CardContent>
           </Card>
  );
};

export default MostAnswersComponent;
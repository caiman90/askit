import React, { Component } from 'react';
import { getHotQuestions,getLatestQuestions } from '../../actions/question.actions';
import { getUsersWithMostAnswers } from '../../actions/auth.actions';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import QuestionComponent from '../questions/QuestionComponent';
import Button from '@material-ui/core/Button';
import MostAnswersComponent from './MostAnswersComponent';
import HeadlineComponent from '../common/HeadlineComponent';

class HomePage extends Component {
  constructor(){
    super();
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount(){
    this.props.getHotQuestions();
    this.props.getUsersWithMostAnswers();
    this.props.getLatestQuestions(false);
  }
  loadMore(){
    this.props.getLatestQuestions(true);
  }

  render() {
    return (
      <div>
          <Grid container justify="center" >
              <div style={{width:'100%'}}>
                  <HeadlineComponent title={'MOST ANSWERS'}/>
                  {this.props.usersWithMostAnswersProvided.map((user,index) =>
                     <MostAnswersComponent user={user} key={index} />
                  )}
              </div>
              <div style={{width:'100%'}}>
                  <HeadlineComponent title={'HOT QUESTIONS'}/>
                  {this.props.hotQuestions.map((question,index) =>
                     <QuestionComponent key={index}  question={question} />
                  )}
              </div>
              <div style={{width:'100%'}}>
                  <HeadlineComponent title={'LATEST QUESTIONS'}/>
                  {this.props.latestQuestions.map((question,index) =>
                     <QuestionComponent key={index}  question={question}                                                                                                                                   />
                  )}
                  <br />
                  <Button style={{marginTop:50}} onClick={this.loadMore}>Load More...</Button>
              </div>
          </Grid>
      </div>
  )}
}
HomePage.propTypes = {
    hotQuestions: PropTypes.array.isRequired,
    usersWithMostAnswersProvided: PropTypes.array.isRequired,
    latestQuestions: PropTypes.array.isRequired,
    auth: PropTypes.object.isRequired,
}


const mapStateToProps = state => ({
    hotQuestions : state.questions.items,
    usersWithMostAnswersProvided: state.auth.usersWithMostAnswersProvided,
    latestQuestions: state.questions.latestItems,
    auth: state.auth.user
});
export default connect(mapStateToProps, { getHotQuestions, getUsersWithMostAnswers, getLatestQuestions })(HomePage);

import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Question from '@material-ui/icons/QuestionAnswer';
import IconButton from '@material-ui/core/IconButton';
import {connect} from 'react-redux';
import { postAnswer } from '../../actions/question.actions';
import Tooltip from '@material-ui/core/Tooltip';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

class AnswerDialog extends React.Component {
  constructor(){
    super();
    this.state = {
      open: false,
      answer: ''
    }
    this.handleAddAnswer = this.handleAddAnswer.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  componentWillMount(){
     ValidatorForm.addValidationRule('minLength', (value) => {
       return value.length < 15 ? false : true;
     });
  }
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  onChange(e){
    this.setState({ [e.target.name]: e.target.value });
  }
  handleAddAnswer(){
    this.props.question.answer = this.state.answer;
    this.props.postAnswer(this.props.question);
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        <Tooltip id="answer" title="Post Answer">
          <IconButton aria-label="Answer"  onClick={this.handleClickOpen}>
            <Question  color="secondary" />
          </IconButton>
        </Tooltip>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Answer question</DialogTitle>
          <DialogContent>
            <DialogContentText>
               {this.props.question.text}
            </DialogContentText>
            <ValidatorForm ref="form" instantValidate={false} onSubmit={this.handleAddAnswer}>
            <TextValidator
              autoFocus
              margin="dense"
              id="answer"
              name="answer"
              value={this.state.answer}
              onChange={this.onChange}
              label="Answer"
              validators={['minLength']}
              errorMessages={['Answer min length is 15 characters']}
              fullWidth
            />
                <Button onClick={this.handleClose} color="primary">
                    Cancel
                </Button>
                <Button type="submit" color="primary">
                    Answer
                </Button>
            </ValidatorForm>
          </DialogContent>
          <DialogActions>

          </DialogActions>
        </Dialog>
      </div>
    );
  }
}


const mapStateToProps = state => ({
    auth: state.auth.user
});

export default connect(mapStateToProps,{ postAnswer })(AnswerDialog);

import React from 'react';
import PropTypes from 'prop-types';
import QuestionComponent from './QuestionComponent';

const QuestionListComponent = ({questions}) => {
  return (
          <div>
            {questions.map((question,index) =>
              <QuestionComponent key={index}  question={question}                                                                                                                                   />
            )}
          </div>
  );
};

QuestionListComponent.propTypes = {
  questions: PropTypes.array.isRequired,
}
export default QuestionListComponent;

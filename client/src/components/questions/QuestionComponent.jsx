import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {connect} from 'react-redux';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ThumbUp from '@material-ui/icons/ThumbUp';
import ThumbDown from '@material-ui/icons/ThumbDown';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AnswerDialog from './AnswerDialog';
import { addLike, unlike } from '../../actions/question.actions';
import Moment from 'react-moment';
import Tooltip from '@material-ui/core/Tooltip';

const styles = theme => ({
  card: {
    width: 300,
    height: 200,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
      width: 40,
      height: 40,
  },
  layout:{
    padding: "0px 5px 5px 0px",
    float: 'left',
  },
  answers:{
    fontSize:14,
    fontWeight: "bold",
  },
  question:{
    "wordWrap": 'break-word',
      "fontFamily": "\"Roboto\", \"Helvetica\", \"Arial\", sans-serif",
      "fontSize": 11,
      "fontWeightLight": 300,
      "fontWeightRegular": 400,
      "fontWeightMedium": 500
  }
});

class QuestionComponent extends React.Component {
  constructor(){
    super();
    this.handleLike = this.handleLike.bind(this);
    this.handleUnlike = this.handleUnlike.bind(this);
  }
  handleLike(){
    this.props.addLike(this.props.question._id);
  }
  handleUnlike(){
    this.props.unlike(this.props.question._id);
  }
  state = { expanded: false };

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  render() {
    const { classes } = this.props;
    let answerDialog;
    if (this.props.auth.isAuthenticated) {
      answerDialog = <AnswerDialog question={this.props.question} />;
    }
    return (
      <div  className={classes.layout}>
        <Card className={classes.card } style={{maxHeight: 300, overflow: 'auto'}}>
          <CardHeader
            avatar={
              <Avatar     alt={this.props.question.author.firstName}
                          src={this.props.question.author.imageUrl}
                          className={classes.avatar}>
              </Avatar>
            }
            action={
              <IconButton>
                <MoreVertIcon />
              </IconButton>
            }
            title={this.props.question.author.firstName + this.props.question.author.lastName}
            subheader={<Moment fromNow>{this.props.question.createdOn}</Moment>}
          />
          <CardContent>
            <Typography component="p" className={classes.question}>
              {this.props.question.text}
            </Typography>
          </CardContent>
          <CardActions className={classes.actions} disableActionSpacing>
            <Tooltip id="like" title="Like">
              <IconButton aria-label="Like" color="primary" onClick={this.handleLike}>
                <ThumbUp /> ({ this.props.question.likes.length })
              </IconButton>
            </Tooltip>
            <Tooltip id="unlike" title="Unlike">
            <IconButton aria-label="Unlike" onClick={this.handleUnlike}>
              <ThumbDown />  ({ this.props.question.unlikes.length })
            </IconButton>
            </Tooltip>
            {answerDialog}
            <IconButton
              className={classnames(classes.expand, {
                [classes.expandOpen]: this.state.expanded,
              })}
              onClick={this.handleExpandClick}
              aria-expanded={this.state.expanded}
              aria-label="Show more"
            >
              <ExpandMoreIcon />
            </IconButton>
          </CardActions>
          <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
            <CardContent>
              <Typography  color="primary" paragraph className={classes.answers}>
                ANSWERS:
              </Typography>
              {this.props.question.answers.map((answer,index) => (
                <div key={index} style={{paddingTop:10}}>
                <Typography color="primary">
                  {answer.author.firstName} {answer.author.lastName}-
                 <Moment fromNow>{answer.createdOn}</Moment>
                </Typography>
                <Typography pharagraph>
                  {answer.text}
                </Typography>
                </div>
              ))}
            </CardContent>
          </Collapse>
        </Card>
      </div>
    );
  }
}

QuestionComponent.propTypes = {
  classes: PropTypes.object.isRequired,
};
const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps,{addLike,unlike})(withStyles(styles)(QuestionComponent));

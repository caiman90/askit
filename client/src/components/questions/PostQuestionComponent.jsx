import React from 'react';
import Button from '@material-ui/core/Button';
import {connect} from "react-redux";
import { postQuestion } from "../../actions/question.actions";
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

class PostQuestionComponent extends React.Component {
  constructor(){
      super();
      this.state = {
          question: ''
      }
      this.onChange = this.onChange.bind(this);
      this.onPostQuestion = this.onPostQuestion.bind(this);
  }
  componentWillMount(){
      ValidatorForm.addValidationRule('minLength', (value) => {
            return value.length < 30 ? false : true;
      });
  }
  onChange(e){
      this.setState({ [e.target.name]: e.target.value });
  }
  onPostQuestion(){
      this.props.postQuestion({ text: this.state.question});
      this.setState({ question: ''});
  }
  render(){
    return (
        <div style={{width:'100%',margin:'0px 0px 40px 0px'}}>
          <ValidatorForm onSubmit={this.onPostQuestion} instantValidate={false}>
          <TextValidator style={{width:'70%'}}
              id="question"
              label="Add Question"
              name="question"
              value={this.state.question}
              onChange={this.onChange}
              margin="normal"
              multiline={true}
              rows={2}
              rowsMax={4}
              validators={['minLength']}
              errorMessages={['Minimum length for question is 30 characters']}
            />
          <Button type="submit" color="primary" style={{width: '20%'}}>Add Question</Button>
          </ValidatorForm>
        </div>
    );
  }
};

export default connect(null, { postQuestion })(PostQuestionComponent);
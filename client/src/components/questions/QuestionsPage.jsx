import React, { Component } from 'react';
import { getQuestions } from '../../actions/question.actions';
import {connect} from 'react-redux';
import QuestionListComponent from './QuestionListComponent';
import PostQuestionComponent from './PostQuestionComponent';
import HeadlineComponent from '../common/HeadlineComponent';
import PropTypes from 'prop-types';

class QuestionsPage extends Component {

  componentDidMount(){
    this.props.getQuestions();
  }
  render() {
    return (
       <div>
          <HeadlineComponent title={'ALL QUESTIONS'}/>
          <PostQuestionComponent />
          <QuestionListComponent questions={this.props.questions} />
       </div>
    );
  }
}



QuestionsPage.propTypes = {
    questions: PropTypes.array.isRequired,
    auth: PropTypes.object.isRequired,
}


const mapStateToProps = state => ({
    questions : state.questions.items,
    auth: state.auth.user
});

export default connect(mapStateToProps,{getQuestions})(QuestionsPage);

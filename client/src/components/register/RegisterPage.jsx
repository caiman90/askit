import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { registerUser } from '../../actions/auth.actions';
import Button from  '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import HeadlineComponent from '../common/HeadlineComponent';

class RegisterPage extends Component {
  constructor() {
    super();
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      repeatPassword: '',
      errorsText: '',
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentWillMount(){
    ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
          if (value !== this.state.password) {
              return false;
          }
          return true;
    });
    ValidatorForm.addValidationRule('minLength', (value) => {
            if (value === undefined || value.length < 6) {
                return false;
            }
            return true;
    });
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.error.length > 0) {
      this.setState({ errorsText: nextProps.auth.error });
    }else if(nextProps.auth.isAuthenticated){
      this.props.history.push('/questions')
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
    if(this.state.errorsText !== ''){
        this.setState({ errorsText: ''});
    }
  }

  onSubmit(e) {
    if(this.state.errorsText !== ''){
        this.setState({ errorsText: ''});
    }
    e.preventDefault();
    let newUser = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      password: this.state.password,
    };
    this.props.registerUser(newUser);
  }

  render() {
    const  formData = this.state;
    let errorScreen = this.state.errorsText !== '' ? <Typography style={{color:'red' ,fontSize:17,border: 1,}}> {this.state.errorsText }</Typography> : '';

    return (
        <Grid container justify="center"  style={{marginTop:40}}>
        <ValidatorForm ref="form" onSubmit={this.onSubmit}  instantValidate={false}>
            <HeadlineComponent title={'Sign Up to AskIt'} />
                <TextValidator
                  placeholder="First Name"
                  name="firstName"
                  value={formData.firstName}
                  onChange={this.onChange}
                  validators={['required']}
                  errorMessages={['This field is required']}
                  margin="normal"
                /> <br /><br /><br />
                <TextValidator
                  placeholder="Last Name"
                  name="lastName"
                  value={formData.lastName}
                  onChange={this.onChange}
                  validators={['required']}
                  errorMessages={['This field is required']}
                />  <br /><br /><br />
                <TextValidator
                  placeholder="Email"
                  name="email"
                  type="email"
                  value={formData.email}
                  onChange={this.onChange}
                  validators={['required', 'isEmail']}
                  errorMessages={['This field is required', 'Email is not valid']}
                 /> <br /><br /><br />
                <TextValidator
                  placeholder="Password"
                  name="password"
                  type="password"
                  value={formData.password}
                  onChange={this.onChange}
                  validators={['required']}
                  errorMessages={['This field is required']}
                /> <br /><br /><br />
                <TextValidator
                  placeholder="Confirm Password"
                  name="passwordRepeat"
                  type="password"
                  value={this.state.passwordRepeat}
                  onChange={this.onChange}
                  validators={['required','isPasswordMatch','minLength']}
                  errorMessages={['This field is required','Password mismatch','Password min length is 6']}
                /> <br /><br /><br />
                <Button type="submit" color="primary" style={{ width: '180px'}}>Register</Button>
                {errorScreen}
              </ValidatorForm>
              </Grid>
    );
  }
}

RegisterPage.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { registerUser })(withRouter(RegisterPage));

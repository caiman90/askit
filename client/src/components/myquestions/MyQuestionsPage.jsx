import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import { getMyQuestions } from '../../actions/question.actions';
import QuestionListComponent from '../questions/QuestionListComponent';
import HeadlineComponent from '../common/HeadlineComponent';

class MyQuestionsPage extends Component {

  componentDidMount(){
    this.props.getMyQuestions();
  }
  render() {
    return (
      <div>
         <HeadlineComponent title={'MY QUESTIONS'}/>
         <QuestionListComponent questions={this.props.myQuestions} />
      </div>
    );
  }
}

MyQuestionsPage.propTypes = {
  myQuestions: PropTypes.array.isRequired,
};
const mapStateToProps = state => ({
    myQuestions : state.questions.items,
    auth: state.auth.user
});
export default connect(mapStateToProps,{getMyQuestions})(MyQuestionsPage);

import React,{ Component } from 'react';
import {
  MuiThemeProvider,
  createMuiTheme,
} from '@material-ui/core/styles';
import './App.css';
import blue from '@material-ui/core/colors/blue';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { setCurrentUser } from '../actions/auth.actions';
import setAuthorizationToken from '../utils/setAuthorizationToken';
import store from '../store/store';
import jwt_decode from 'jwt-decode';
import PersistentDrawer from './navigation/PersistentDrawer';

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: {
      main: '#11cb5f',
    },
  },
  typography: {
     fontSize: 12,
   },
});

if (localStorage.jwtToken) {
  setAuthorizationToken(localStorage.jwtToken);
  const decoded = jwt_decode(localStorage.jwtToken);
  store.dispatch(setCurrentUser(decoded));
}

class App extends Component {
  render(){
  return (
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <BrowserRouter>
           <PersistentDrawer />
        </BrowserRouter>
      </MuiThemeProvider>
    </Provider>
    );
}
}
export default App;

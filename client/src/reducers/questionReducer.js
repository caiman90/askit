import * as types from '../actions/types';

const defaultState = {
  items: [],
  item: {},
  error: '',
  latestItems: [],
};
export default (state = defaultState, action) => {
  switch (action.type) {
    case types.LOAD_QUESTIONS_SUCCESS:
      return { ...state, items: action.questions };
    case types.LOAD_LATEST_QUESTIONS_SUCCESS:
      return { ...state, latestItems: action.latestQuestions };
    case types.LOAD_HOT_QUESTIONS_SUCCESS:
      return { ...state, items: action.hotQuestions };
    case types.LOAD_MY_QUESTIONS_SUCCESS:
      return { ...state, items: action.myQuestions };
    case types.POST_QUESTION_SUCCESS:
      return { ...state, items: [...state.items, action.question] };
    case types.POST_ANSWER_SUCCESS:
      return {
        ...state,
        items: state.items.map((question) => {
          if (question._id === action.answer.question_id) {
            return { ...question, answers: [...question.answers, action.answer] };
          }
          return question;
        }),
      };
    case types.POST_LIKE_SUCCESS:
      return {
        ...state,
        items: state.items.map((question) => {
          if (question._id === action.question._id) {
            return { ...question, likes: action.question.likes }
          }
          return question;
        }),
      };
    case types.POST_UNLIKE_SUCCESS:
      return {
        ...state,
        items: state.items.map((question) => {
          if (question._id === action.question._id) {
            return { ...question, unlikes: action.question.unlikes }
          }
          return question;
        }),
      };
    case types.UPDATE_LIKES_AND_UNLIKES:
      return {
        ...state,
        items: state.items.map((question) => {
          if (question._id === action.question._id) {
            return { ...question, unlikes: action.question.unlikes, likes: action.question.likes }
          }
          return question;
        }),
      };
    case types.ERROR:
      return { ...state, error: action.error };
    default:
      return state;
  }
};

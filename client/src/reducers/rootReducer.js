import { combineReducers } from 'redux';
import questionReducer from './questionReducer';
import authReducer from './authReducer';

export default combineReducers({
  questions: questionReducer,
  auth: authReducer,
});

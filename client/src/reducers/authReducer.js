import {
  SET_CURRENT_USER, AUTH_ERROR,
  USERS_WITH_MOST_ANSWERS_PROVIDED,
  RESET_PASSWORD, RESET_PASSWORD_LINK, PROFILE_DATA_SUCCESS, USER_IMAGE_UPLOAD
} from '../actions/types';
import isEmpty from '../validation/is-empty';

const defaultState = {
  user: {},
  isAuthenticated: false,
  error: '',
  usersWithMostAnswersProvided: [],
  info: '',
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return { ...state, user: action.user, isAuthenticated: !isEmpty(action.user), error: '' };
    case USERS_WITH_MOST_ANSWERS_PROVIDED:
      return { ...state, usersWithMostAnswersProvided: action.users };
    case RESET_PASSWORD:
      return { ...state, info: action.message };
    case RESET_PASSWORD_LINK:
      return { ...state, info: action.message };
    case PROFILE_DATA_SUCCESS:
      return { ...state, user: action.user, isAuthenticated: !isEmpty(action.user) };
    case USER_IMAGE_UPLOAD:
      return { ...state, user: action.user };
    case AUTH_ERROR:
      return { ...state, error: action.error };
    default:
      return state;
  }
};

import axios from 'axios';
import * as types from './types';

// ERROR HANDLING SHOULD BE CENTRALIZED

const loadQuestionsSuccess = questions => ({ type: types.LOAD_QUESTIONS_SUCCESS, questions });
const loadLatestQuestionsSuccess = latestQuestions => ({ type: types.LOAD_LATEST_QUESTIONS_SUCCESS, latestQuestions });
const loadHotQuestionsSuccess = hotQuestions => ({ type: types.LOAD_HOT_QUESTIONS_SUCCESS, hotQuestions });
const loadMyQuestionsSuccess = myQuestions => ({ type: types.LOAD_MY_QUESTIONS_SUCCESS, myQuestions });
const postQuestionSuccess = question => ({ type: types.POST_QUESTION_SUCCESS, question });
const postAnswerSuccess = answer => ({ type: types.POST_ANSWER_SUCCESS, answer });
const postLikeSuccess = question => ({ type: types.POST_LIKE_SUCCESS, question });
const unlikeSuccess = question => ({ type: types.POST_UNLIKE_SUCCESS, question });
const updateLikesAndUnlikes = question => ({ type: types.UPDATE_LIKES_AND_UNLIKES, question });
const errorOccured = error => ({ type: types.ERROR, error });


export const getQuestions = () => (dispatch) => {
  axios.get('/api/questions').then((res) => {
    dispatch(loadQuestionsSuccess(res.data));
  }).catch(() => {
    dispatch(errorOccured([]));
  });
};

export const getLatestQuestions = loadMore => (dispatch) => {
  axios.get(`/api/latestquestions/${loadMore}`).then((res) => {
    dispatch(loadLatestQuestionsSuccess(res.data));
  }).catch(() => {
    dispatch(errorOccured([]));
  });
};

export const getMyQuestions = () => (dispatch) => {
  axios.get('/api/myQuestions').then((res) => {
    dispatch(loadMyQuestionsSuccess(res.data));
  }).catch(() => {
    dispatch(errorOccured([]));
  });
};
export const getHotQuestions = () => (dispatch) => {
  axios.get('/api/questions/hot').then((res) => {
    dispatch(loadHotQuestionsSuccess(res.data));
  }).catch(() => {
    dispatch(errorOccured([]));
  });
};

export const postQuestion = question => (dispatch) => {
  axios.post('/api/question', question).then((res) => {
    dispatch(postQuestionSuccess(res.data));
  }).catch(() => {
    dispatch(errorOccured({}));
  });
};

export const postAnswer = question => (dispatch) => {
  axios.post(`/api/${question._id}/answer`, { answer: question.answer }).then((res) => {
    res.data.question_id = question._id;
    dispatch(postAnswerSuccess(res.data));
  }).catch(() => {
    dispatch(errorOccured({}));
  });
};
export const addLike = id => (dispatch) => {
  axios.post(`/api/questions/like/${id}`)
    .then((res) => {
    res.data.bothItemsUpdated ?
     dispatch(updateLikesAndUnlikes(res.data.question)) : dispatch(postLikeSuccess(res.data.question));


    }).catch(err => dispatch(errorOccured(err)));
};
export const unlike = id => (dispatch) => {
  axios.post(`/api/questions/unlike/${id}`)
    .then((res) => {
      res.data.bothItemsUpdated ?
       dispatch(updateLikesAndUnlikes(res.data.question)) : dispatch(unlikeSuccess(res.data.question));

    }).catch(err => dispatch(errorOccured(err)));
};

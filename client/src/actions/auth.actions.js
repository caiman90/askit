import axios from 'axios';
import jwt_decode from 'jwt-decode';
import {
  SET_CURRENT_USER,
  AUTH_ERROR, USERS_WITH_MOST_ANSWERS_PROVIDED,
  RESET_PASSWORD_LINK, RESET_PASSWORD, PROFILE_DATA_SUCCESS, USER_IMAGE_UPLOAD
} from './types';
import setAuthorizationToken from '../utils/setAuthorizationToken';

export const setCurrentUser = user => ({ type: SET_CURRENT_USER, user });
const loadUsersWithMostAnswersProvided = users => ({ type: USERS_WITH_MOST_ANSWERS_PROVIDED, users });
const resetPasswordLinkSent = message => ({ type: RESET_PASSWORD_LINK, message });
const resetPasswordSent = message => ({ type: RESET_PASSWORD, message });
const profileDataSuccess = user => ({ type: PROFILE_DATA_SUCCESS, user });
const imageUpload = user => ({ type: USER_IMAGE_UPLOAD, user });
const authError = error => ({ type: AUTH_ERROR, error });


export const logout = () => (dispatch) => {
  localStorage.removeItem('jwtToken');
  setAuthorizationToken(false);
  dispatch(setCurrentUser({}));
};

export const login = data => (dispatch) => {
  axios.post('/auth/login', data).then((res) => {
    localStorage.setItem('jwtToken', res.data.token);
    setAuthorizationToken(res.data.token);
    const decoded = jwt_decode(localStorage.jwtToken);
    dispatch(setCurrentUser(decoded));
  }).catch((error) => {
    dispatch(authError(error.response.data))
  });
}
export const registerUser = data => (dispatch) => {
  axios.post('/auth/register', data).then((res) => {
    localStorage.setItem('jwtToken', res.data.token);
    setAuthorizationToken(res.data.token);
    const decoded = jwt_decode(localStorage.jwtToken);
    dispatch(setCurrentUser(decoded));
  }).catch((error) => {
    dispatch(authError(error.response.data.message))
  });
}

export const getUsersWithMostAnswers = () => (dispatch) => {
  axios.get('/auth/users/most-answers').then((res) => {
    dispatch(loadUsersWithMostAnswersProvided(res.data));
  }).catch((error) => {
    dispatch(authError(error.data))
  });
}

export const getProfileData = () => (dispatch) => {
  axios.get('/auth/profile').then((res) => {
    dispatch(profileDataSuccess(res.data));
  }).catch((error) => {
    dispatch(authError(error.data))
  });
}
export const resetPasswordLink = email => (dispatch) => {
  axios.put('/auth/resetpasswordlink', { email }).then((res) => {
    dispatch(resetPasswordLinkSent(res.data));
  }).catch((error) => {
    dispatch(authError(error.response.data.message))
  });
}
export const resetPassword = requestData => (dispatch) => {
  axios.post('/auth/resetpassword', requestData).then((res) => {
    dispatch(resetPasswordSent(res.data));
  }).catch((error) => {
    dispatch(authError(error.response.data.message))
  });
}
export const uploadUserImage = img => (dispatch) => {
    const formData = new FormData()
    formData.append('userImage', img, img.name)
    axios.post('/auth/image',formData).then((res) => {
        dispatch(imageUpload(res.data));
    }).catch((error) => {
        dispatch(authError(error.response.data.message))
    });
}

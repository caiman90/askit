const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

const userSchema = new mongoose.Schema({
    email: { type:String, unique: true },
    password: String,
    firstName:String,
    lastName:String,
    description:String,
    imageUrl:String,
    resetToken:String,
    num_of_answers: {type:Number, default:0},
    num_of_questions: {type:Number, default:0}
});

userSchema.pre('save',function(next){
    var user = this;
    if(!user.isModified('password')) return next();
    bcrypt.hash(user.password,null,null,(err,hash) => {
        if(err) return next(err);
        user.password = hash
        next();
    });
});

module.exports = mongoose.model('User',userSchema);

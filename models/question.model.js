const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const answerSchema = new Schema({
   text: String,
   author: {type: Schema.Types.ObjectId, ref: 'User'},
   createdOn: { type: Date, default: Date.now },
});

const likeSchema = new Schema({
   user: { type: Schema.Types.ObjectId, ref: 'User' }
});


const questionSchema = new Schema({
   text: String,
   author: {type: Schema.Types.ObjectId, ref: 'User'},
   answers: [answerSchema],
   createdOn:{ type: Date, default: Date.now },
   unlikes:[likeSchema],
   likes:[likeSchema],
})

module.exports = mongoose.model('Question',questionSchema);

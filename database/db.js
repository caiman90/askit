const mongoose = require('mongoose')
mongoose.Promise = Promise;
const MONGO_URL = process.env.MONGO_DB_URL || 'YourURL';
module.exports = () => {
    mongoose.connect(MONGO_URL,{ useNewUrlParser: true }, (err) => {
        if(!err)
            console.log('Succesfully connected to askit database');
        else
            console.log('Error occured on connectiong askit database. Cause: ' + err);
    });
}

const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
require('dotenv').config();

const PORT_NUMBER = process.env.PORT || 4000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const authRoutes = require('./routes/auth.controller');
const questionRoutes = require('./routes/questions.controller');

require('./database/db')();

app.use('/api',questionRoutes);
app.use('/auth',authRoutes);
app.use('/images', express.static('images'));

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'));
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
};

app.listen(PORT_NUMBER,() => console.log('STARTED SERVER AT PORT: ' + PORT_NUMBER));

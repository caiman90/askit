# askIT

Small web application where users can register and, upon successful registration, ask questions.
All the other registered users can provide the answers. Also, users can rate the questions and
answers (thumbs up/down or like/dislike)

# PURPOSE
 Job interview. To show technical skills with using upper technologies


# KEY FEATURES
  - Post questions, All questions displayed with answering option
  - MyQuestions ( only my questions are displayed )
  - Login , Register and Reset Password functionality
  - Most Active users, latest Questions, Hot Questions
  - Like / Unlike questions

# TECHNOLOGY STACK
  # Backend:
  - NodeJS + Express
  - Mongo
  - JWT for protected routes

  # Frontend:
  - React.js
  - Redux
  - Material-ui

# Intructions how to run web application :
  * Prerequisites : NodeJS

  1. Clone the project

  2. Navigate to  /askit/client
    - run npm install
    - run npm start  

  3. Navigate to /askit/server
    - run npm install
    - run npm start

 # For local development you ll need .env file with following values :

  SECRET_KEY= // your own secret key
  NODEMAILER_USERNAME= // your own email address
  NODEMAILER_PASSWORD= // your email address password
  NODEMAILER_SERVICE= // Example : gmail etc...
  MONGO_DB_URL= // For ex use mlab to create free db and provide link here
  RESET_PW_URL='http://localhost:3000/reset'


  4. To run concurrently client and server please use this command :

   - npm run dev

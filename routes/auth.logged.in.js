const jwt = require('jsonwebtoken');
const SECRET_KEY = process.env.SECRET_KEY || 'secretkey';

module.exports = function isUserLoggedId(req,res,next){
  const header = req.headers['authorization'];
  if(typeof header !== 'undefined') {
     const args = header.split(' ');
     const token = args[1];
     jwt.verify(token, SECRET_KEY, (error,authData) => {
       if(error){
          req.user = null;
          next();
       }else{
         req.user = authData.user;
         next();
       }
     });
  } else {
    req.user = null;
    next();

  }
}

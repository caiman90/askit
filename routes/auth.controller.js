const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');
const User = require('../models/user.model.js');
const express = require('express');
const verifyAuth = require('./auth.middleware');
const nodemailer = require('nodemailer');
const router = express.Router();
const multer = require('multer');

const storage = multer.diskStorage({
   destination: (req,file,cb) => {
       cb(null,'./images');
   },
   filename: (req,file,cb) => {
       cb(null,file.originalname);
   }
});

const fileFilter = (req, file, cb) => {
    console.log(file)
    if (file.mimetype === 'image/jpg'|| file.mimetype === 'image/jpeg'|| file.mimetype === 'image/png'){
        cb(null, true);
    }else{
        cb('Mime type not supported', false);
    }

}

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});


const SECRET_KEY = process.env.SECRET_KEY || 'secretkey';
const NM_USERNAME = process.env.NODEMAILER_USERNAME || 'YourUsr';
const NM_PASSWORD = process.env.NODEMAILER_PASSWORD || 'YourPwd';
const NM_SERVICE = process.env.NODEMAILER_SERVICE || 'YourServicePwd';
const RESET_PW_URL = process.env.RESET_PW_URL || 'http://localhost:3000/reset/';

let transporter = nodemailer.createTransport({service: NM_SERVICE,
  auth: {
    user: NM_USERNAME,
    pass: NM_PASSWORD
  }});

    router.post('/register',function(req, res) {
         const registerData = req.body;
         const user = new User(registerData);
         user.save((err,newUser) => {
            if(err)
             res.status(500).send({ message: 'Duplicate user: '+ req.body.email});

              jwt.sign({ user:{email:user.email,firstName:user.firstName,lastName:user.lastName, id:user._id} }, SECRET_KEY, (err,token) => {
              res.status(200).send({ token:token });
            });
         });
    });

    router.post('/login',async (req, res) => {
        const loginData = req.body;
        const user = await User.findOne({email: loginData.email})

         if(!user)
             return res.status(401).send({message: 'Email or password invalid'});

        bcrypt.compare(loginData.password,user.password,(err,isMatch)=> {
        if(!isMatch)
            return res.status(401).send({message: 'Email or password invalid'});

        jwt.sign({ user:{email:user.email,firstName:user.firstName,lastName:user.lastName, id:user._id, imageUrl: user.imageUrl}}, SECRET_KEY,(err,token) => {
            res.status(200).send({token:token,});
        });
      });
    });

    router.get('/profile',verifyAuth,async (req, res) =>{
        try {
            const user = await User.findById(req.user.id,'-password -__v')
            res.status(200).send(user)
        }catch (error){
            res.status(500).json({message:"Error retrieving user profile" + error})
        }
    });

    router.get('/users/most-answers', async(req,res) => {
      try {
          const mostActiveUsers = await User.find({}).where('_id').ne('5b4c5a66987a7a1a1ec89cfc').sort({num_of_answers: -1}).limit(5);
          res.send(mostActiveUsers);
      }catch (error){
          res.status(500).json({message:"Internal Server error occured" + error});
      }

    });

	router.put('/resetpasswordlink',(req, res) => {
    User.findOne({ email: req.body.email },(err, user) => {
      if (err) throw err;

      if (user) {
        user.resetToken = jwt.sign({ username: user.username, email: user.email }, SECRET_KEY, { expiresIn: '24h' });
        user.save(function(err) {
          if (err) {
            res.status(400).json({message: err});
          } else {
						transporter.sendMail({
    					from: 'askitmop@gmail.com',
    					to: req.body.email,
    					subject: 'Reset password',
    					html: 'Hello ' + user.firstName + ', <br/> You have recently requested a password reset link. Please click on the link below to reset your password:<br><br><a  href="'+RESET_PW_URL + user.resetToken +'">'+RESET_PW_URL+'</a><br/><br/>' + 'Thanks </br> askitmop team'
						}, (err, info) => {
							res.json({ message: 'Please check your e-mail for password reset link' });
						});
          }
        });
      }else{
				res.status(400).json({message: 'Username was not found' });
			}
    });

	});

	router.post('/resetpassword', function(req, res) {
		User.findOne({ resetToken: req.body.token },(err, user) => {
			if (err) throw err;
			var token = req.body.token;
			jwt.verify(token, SECRET_KEY, function(err, decoded) {
				if (err) {
					res.status(400).json({  message: 'Password link has expired' + err });
				} else {
					if (!user) {
						res.status(400).json({ message: 'Password link not valid' });
					} else {
						user.resetToken = '';
						user.password = req.body.password;
						user.save((err,updatedUser) => {
							res.json({ success: true });
						});
					}
				}
			});
		});
	});

	router.post('/image',upload.single('userImage'),verifyAuth,(req,res) => {
        User.findByIdAndUpdate( req.user.id , {imageUrl: req.file.path},{projection: { "password" : 0},new: true }, (err,doc)=>{
            if(err)
              res.status(500).send(err);
            res.status(200).send(doc);
        });
    });

module.exports = router;

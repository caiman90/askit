const jwt = require('jsonwebtoken');
const SECRET_KEY = process.env.SECRET_KEY || 'secretkey';

module.exports = function verifyToken(req,res,next){
  const header = req.headers['authorization'];
  if(typeof header !== 'undefined') {
     const args = header.split(' ');
     const token = args[1];
     jwt.verify(token, SECRET_KEY, (error,authData) => {
       if(error){
         res.sendStatus(403);
       }else{
         req.user = authData.user;
         next();
       }
     });
  } else {
    res.sendStatus(403);
  }
}

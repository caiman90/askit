const express = require("express");
const verifyToken = require("./auth.middleware");
const Question = require("../models/question.model");
const User = require('../models/user.model.js');
const router = express.Router();

router.get('/questions', async(req,res) => {
  try {
      const questions = await Question.find({}).populate('author','-_id -__v -password').populate('answers.author','-_id -__v -password');
      res.send(questions)
  }catch (error){
      res.status(500).json({message: "Internal Server error occured" + error});
  }
});
router.get('/latestQuestions/:loadMore', async(req, res) => {
   let loadMore = req.params.loadMore;
   let questions;
    try {
      if(loadMore === 'true'){
          questions = await Question.find({}).sort({date: -1}).populate('author','-_id -__v -password').populate('answers.author','-_id -__v -password');
      }else{
          questions = await Question.find({}).sort({date: -1}).limit(20).populate('author','-_id -__v -password').populate('answers.author','-_id -__v -password');;
      }
      res.send(questions)
  }catch (error){
      res.status(500).json({message: "Internal Server error occured" + error});
  }
});

router.get('/myQuestions',verifyToken, async(req, res) => {
  try {
      const questions = await Question.find({'author': req.user.id}).sort({createdOn: -1}).populate('author','-_id -__v -password').populate('answers.author','-_id -__v -password');
      res.send(questions)
  }catch (error){
      res.status(500).json({message: "Internal Server error occured" + error});
  }

});

router.get('/questions/hot', async(req,res) => {
  try {
    const questions = await Question.aggregate([
        { $project: {
            "author": 1,
            "answers": 1,
            "likes":1,
            "unlikes":1,
            "text":1,
            "createdOn":1,
            "length": { "$size": "$likes" }
        }},
        {  $lookup: {
                from: 'users',
                localField: 'author',
                foreignField: '_id',
                as: 'author'
            }
          },
        {$unwind: '$author'},
        { "$sort": { "length": -1 } },
        { "$limit": 5 }
    ]);
    res.send(questions);
  }catch (error){
    res.status(500).json({message: "Internal Server error occured" + error});
  }
});

router.post('/question',require('./auth.logged.in'),(req, res) => {
      const questionToAdd = req.body;
      // if it is anonymus user posting question hard wire id of anonymus user
      questionToAdd.author = req.user ? req.user.id : "5b4c5a66987a7a1a1ec89cfc";
      const question = new Question(questionToAdd);
      question.save((err,addedQuestion) => {
        if(err)
          res.status(500).send({ message: 'Error saving question' + err});

        if(req.user){
          addedQuestion.populate('author','-_id -__v -password').execPopulate();
          User.findByIdAndUpdate( req.user.id , {$inc:{num_of_questions:1}}, (err,doc)=>{
          res.status(200).send(addedQuestion);
          });
        }else{
          addedQuestion.populate('author','-_id -__v -password').execPopulate().then(()=>{
            res.status(200).send(addedQuestion);
          });


        }
     });
});

router.post('/:id/answer',verifyToken,(req,res) => {
    Question.findById(req.params.id, (err, question) =>{
        const answer = { text: req.body.answer , author: req.user.id };
        question.answers.push(answer);
        question.save((err,newQuestion) => {
            if(err)
                res.status(500).send({ message: 'Error saving question' + err});

            User.findByIdAndUpdate( req.user.id , {$inc:{num_of_answers:1}}, (err,doc)=>{
                res.status(200).send(answer);
            });
        })
    });
});

router.post('/questions/like/:id',verifyToken, (req, res) => {
      Question.findById(req.params.id)
        .then(question => {
          question.likes.filter(like => like.user.toString() === req.user.id).length > 0 ?
              removeItem(question,'likes',req.user.id,res) : addItem(question,'likes','unlikes',req.user.id,res);
        })
        .catch(err => res.status(404).json({ questionNotFound: 'Question not found' , error: err }));
  }
);

router.post('/questions/unlike/:id',require('./auth.middleware'),(req, res) => {
      Question.findById(req.params.id)
        .then(question => {
          question.unlikes.filter(unlike => unlike.user.toString() === req.user.id).length > 0 ?
              removeItem(question,'unlikes',req.user.id,res) : addItem(question,'unlikes','likes',req.user.id,res);
        })
        .catch(err => res.status(404).json({ questionNotFound: 'Question not found', error: err }));
  }
);

const removeItem = (question,property,userId,res) => {
  const removeIndex = question[property].map(item => item.user.toString()).indexOf(userId);
  question[property].splice(removeIndex, 1);
  question.save().then(updatedQuestion => res.json({question:updatedQuestion,bothItemsUpdated:false}));
};

const addItem = (question, addTo, remove, userId, res) => {
  let bothItemsUpdated = false;
  question[addTo].push({ user: userId });
  if(question[remove].filter(item => item.user.toString() === userId).length > 0) {
    const removeIndex = question[remove].map(item => item.user.toString()).indexOf(userId);
    question[remove].splice(removeIndex, 1);
    bothItemsUpdated = true;
  }
  question.save().then(updatedQuestion => {
    res.json({question:updatedQuestion,bothItemsUpdated:bothItemsUpdated})
  });
};

module.exports = router;
